package tb.sockets.client;

import java.awt.EventQueue;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JFormattedTextField;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.border.LineBorder;

public class MainFrame extends JFrame implements ActionListener{
	private JPanel contentPane;
	Game game;
	Thread tGame;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public MainFrame() throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 26, 14);
		contentPane.add(lblHost);
		
		JFormattedTextField frmtdtxtfldIp = new JFormattedTextField();
		frmtdtxtfldIp.setBounds(43, 11, 90, 20);
		frmtdtxtfldIp.setText("");
		contentPane.add(frmtdtxtfldIp);
		
		JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField();
		frmtdtxtfldXxxx.setText("");
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);
		
		JLabel lblNotConnected;
		lblNotConnected = new JLabel("Not Connected");
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(Color.RED);
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 26, 14);
		contentPane.add(lblPort);
		
		JLabel lblPlayers = new JLabel("Current players: ");
		lblPlayers.setBounds(10, 150, 150, 14);
		contentPane.add(lblPlayers);
		
		JLabel lblResult = new JLabel("");
		lblResult.setBounds(10, 200, 150, 14);
		contentPane.add(lblResult);
		
		Map panel = new Map(16, 16);
		panel.setBounds(145, 14, 487, 448);
		panel.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel.setLayout(new GridLayout(16, 16, 0, 0));
		contentPane.add(panel);
		
		game = new Game(panel);
		
		JButton btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!Konsola.isConnected())
				{
					Konsola.Connect(frmtdtxtfldIp.getText(), Integer.parseInt(frmtdtxtfldXxxx.getText()));
				}
				if(Konsola.isConnected())
				{
					lblNotConnected.setText("Connected");
					lblNotConnected.setForeground(new Color(255, 255, 255));
					lblNotConnected.setBackground(Color.GREEN);
					lblNotConnected.setOpaque(true);
					tGame = new Thread(game);
					tGame.start();
					
				}
			}
		});
		btnConnect.setBounds(10, 70, 125, 23);
		contentPane.add(btnConnect);
		
		Timer timer = new Timer(10, this);
		timer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblPlayers.setText("Current players: " + game.playersConnected);
				if(game.isLost)
				{
					lblResult.setText("You lost!! :(");
					lblResult.setForeground(Color.RED);
				}
				if(game.isWin)
				{
					lblResult.setText("You win!! :)");
					lblResult.setForeground(Color.GREEN);
				}
			}
		});
		timer.start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}
}
