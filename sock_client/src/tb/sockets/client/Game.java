package tb.sockets.client;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.TreeSet;

import tb.sockets.client.kontrolki.Field.Type;

public class Game implements Runnable {
	private boolean isYourTurn = false;
	private int bombsAmount = 0;
	public int playersConnected = 0;
	public boolean isLost = false;
	public boolean isWin = false;
	
	Map map;
	Game(Map _map)
	{
		map = _map;
	}
	@Override
	public void run() {
		try {
			bombsAmount = Konsola.getInt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("BombsAmount: " + bombsAmount);
		try {
			map.init(Konsola.getBombs(bombsAmount));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			isYourTurn = Konsola.getBoolean();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		do
		{
			try {
				playersConnected = Konsola.getInt();
				System.out.println(playersConnected);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}while(playersConnected < 2);
		
		map.UnBlock();
		while(true)
		{
			map.Update();
			if(isYourTurn)
			{
				if(!isWin && !isLost)
					map.UnBlock();
				for(int i = 0; i < 256; i++)
				{
					if(map.fieldlist.get(i).isClicked)
					{
						map.fieldlist.get(i).show();
						map.fieldlist.get(i).isClicked = false;
						try {
							Konsola.SendInt(i);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(map.fieldlist.get(i).getFieldType() == Type.BOMB)
						{
							isLost = true;
							map.Block();
						}
						isYourTurn = false;
						break;
					}
				}
			}
			else
			{
				map.Block();
				int clickId = 0;
				try {
					clickId = Konsola.getInt();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				isYourTurn = true;
				map.fieldlist.get(clickId).show();
				map.fieldlist.get(clickId).isClicked = false;
				if(map.fieldlist.get(clickId).getFieldType() == Type.BOMB)
				{
					isWin = true;
					map.Block();
				}
			}
			map.Update();
		}
	}
}

