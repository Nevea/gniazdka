package tb.sockets.client;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;

import tb.sockets.client.kontrolki.Field;
import tb.sockets.client.kontrolki.Field.Type;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.Random;
import java.util.TreeSet;

public class Map extends JPanel implements ActionListener{
	
	public LinkedList<Field> fieldlist = new LinkedList<Field>();
	
	public int undiscovered = 0;
	
	public Map(int sizeX, int sizeY) {
		setBackground(new Color(255, 255, 240));
		createMap(sizeX, sizeY);
		
	}
	private void createMap(int sizeX, int sizeY)
	{
		for(int i = 0; i < sizeX*sizeY; i++)
		{
			fieldlist.add(new Field());
			fieldlist.get(i).addActionListener(this);
			this.add(fieldlist.get(i));
		}
	}
	
	public void Block()
	{
		for(Field i: fieldlist)
			i.allowed = false;
	}
	
	public void UnBlock()
	{
		for(Field i: fieldlist)
			i.allowed = true;
	}
	public void Update()
	{
		do
		{
			undiscovered = 0;
			for(int i = 0; i < 256; i++)
			{
				expand(i);
			}
		}while(undiscovered != 0);
	}
	
	public void init(TreeSet<Integer> bombsPosition)
	{
		setBombs(bombsPosition);
		addNumbers();
	}
	
	private void setBombs(TreeSet<Integer> bombsPosition)
	{
		try{
			for(Integer i: bombsPosition)
			{
				fieldlist.get(i.intValue()).setFieldType(Type.BOMB);
			}
		}catch(NullPointerException e){}
		
	}
	
	private void addNumbers()
	{
		for(int i = 0; i < 256; i++)
		{
			Integer bombCounter = 0;
			if(fieldlist.get(i).getFieldType() != Type.BOMB)
			{
				try
				{
					if(i%16!=0)
					{
						if(fieldlist.get(i-1) != null && fieldlist.get(i-1).isBomb())  // Lewo od pola
						{
							bombCounter++;
						}
					}
				} catch (IndexOutOfBoundsException e) {}
				
				try
				{
					if(i%15!=0)
					{
						if(fieldlist.get(i+1) != null && fieldlist.get(i+1).isBomb()) // Prawo od pola
						{
							bombCounter++;
						}
					}
				} catch (IndexOutOfBoundsException e) {}
				
				try
				{
					if(fieldlist.get(i-0-16) != null && fieldlist.get(i-0-16).isBomb()) // G�ra od pola
					{
						bombCounter++;
					}
				} catch (IndexOutOfBoundsException e) {}

				try
				{
					if(fieldlist.get(i+16) != null && fieldlist.get(i+16).isBomb()) // D� od pola
					{
						bombCounter++;
					}
				} catch (IndexOutOfBoundsException e) {}

				try
				{
					if(i%16!=0)
					{
						if(fieldlist.get(i-1-16) != null && fieldlist.get(i-1-16).isBomb()) // Lewy-g�rny od pola
						{
							bombCounter++;
						}
					}
				} catch (IndexOutOfBoundsException e) {}
				
				try
				{
					if(i%15!=0)
					{
						if(fieldlist.get(i+1-16) != null && fieldlist.get(i+1-16).isBomb()) // Prawy-g�rny od pola
						{
							bombCounter++;
						}
					}
				} catch (IndexOutOfBoundsException e) {}
				
				try
				{
					if(i%16!=0)
					{
						if(fieldlist.get(i-1+16) != null && fieldlist.get(i-1+16).isBomb()) // Lewy-dolny od pola
						{
							bombCounter++;
						}
					}
				} catch (IndexOutOfBoundsException e) {}
				
				try
				{
					if(i%15!=0)
					{
						if(fieldlist.get(i+1+16) != null && fieldlist.get(i+1+16).isBomb()) // Prawy-dolny od pola
						{
							bombCounter++;
						}
					}
				} catch (IndexOutOfBoundsException e) {}
				
			}
			if(bombCounter != 0)
				fieldlist.get(i).setNumber(bombCounter);
		}
	}
	public void expand(int index)
	{
		if(fieldlist.get(index).getFieldType() == Type.EMPTY && !fieldlist.get(index).hidden)
		{
			try
			{
				if(index%16!=0)
				{
					if(fieldlist.get(index-1) != null && fieldlist.get(index-1).hidden  && (fieldlist.get(index-1).getFieldType() == Type.EMPTY || fieldlist.get(index-1).getFieldType() == Type.NUMBER))  // Lewo od pola
					{
						fieldlist.get(index-1).show();
						undiscovered++;
					}
				}
			} catch (IndexOutOfBoundsException e) {}
			try
			{
				if(index%15!=0)
				{
					if(fieldlist.get(index+1) != null && fieldlist.get(index+1).hidden  && (fieldlist.get(index+1).getFieldType() == Type.EMPTY || fieldlist.get(index+1).getFieldType() == Type.NUMBER)) // Prawo od pola
					{
						fieldlist.get(index+1).show();
						undiscovered++;
					}
				}
			} catch (IndexOutOfBoundsException e) {}
			try
			{
				if(fieldlist.get(index-0-16) != null && fieldlist.get(index-0-16).hidden  && (fieldlist.get(index-0-16).getFieldType() == Type.EMPTY || fieldlist.get(index-0-16).getFieldType() == Type.NUMBER)) // G�ra od pola
				{
					fieldlist.get(index-0-16).show();
					undiscovered++;
				}
			} catch (IndexOutOfBoundsException e) {}

			try
			{
				if(fieldlist.get(index+16) != null && fieldlist.get(index+16).hidden  && (fieldlist.get(index+16).getFieldType() == Type.EMPTY || fieldlist.get(index+16).getFieldType() == Type.NUMBER)) // D� od pola
				{
					fieldlist.get(index+16).show();
					undiscovered++;
				}
			} catch (IndexOutOfBoundsException e) {}

			try
			{
				if(index%16!=0)
				{
					if(fieldlist.get(index-1-16) != null && fieldlist.get(index-1-16).hidden  && (fieldlist.get(index-1-16).getFieldType() == Type.EMPTY || fieldlist.get(index-1-16).getFieldType() == Type.NUMBER)) // Lewy-g�rny od pola
					{
						fieldlist.get(index-1-16).show();
						undiscovered++;
					}
				}
			} catch (IndexOutOfBoundsException e) {}
			
			try
			{
				if(index%15!=0)
				{
					if(fieldlist.get(index+1-16) != null && fieldlist.get(index+1-16).hidden  && (fieldlist.get(index+1-16).getFieldType() == Type.EMPTY || fieldlist.get(index+1-16).getFieldType() == Type.NUMBER)) // Prawy-g�rny od pola
					{
						fieldlist.get(index+1-16).show();
						undiscovered++;
					}
				}
			} catch (IndexOutOfBoundsException e) {}
			
			try
			{
				if(index%16!=0)
				{
					if(fieldlist.get(index-1+16) != null && fieldlist.get(index-1+16).hidden  && (fieldlist.get(index-1+16).getFieldType() == Type.EMPTY || fieldlist.get(index-1+16).getFieldType() == Type.NUMBER)) // Lewy-dolny od pola
					{
						fieldlist.get(index-1+16).show();
						undiscovered++;
					}
				}
			} catch (IndexOutOfBoundsException e) {}
			
			try
			{
				if(index%15!=0)
				{
					if(fieldlist.get(index+1+16) != null && fieldlist.get(index+1+16).hidden  && (fieldlist.get(index+1+16).getFieldType() == Type.EMPTY || fieldlist.get(index+1+16).getFieldType() == Type.NUMBER)) // Prawy-dolny od pola
					{
						fieldlist.get(index+1+16).show();
						undiscovered++;
					}
				}
			} catch (IndexOutOfBoundsException e) {}
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}