package tb.sockets.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.TreeSet;

public class Konsola {
	static Socket sock = null;
	static void SendMessage(String text) throws IOException
	{
		DataOutputStream so = new DataOutputStream(sock.getOutputStream());
		so.writeChars(text);
		so.flush();
	}
	
	static void SendInt(int index) throws IOException
	{
		System.out.println("Sending");
		DataOutputStream so = new DataOutputStream(sock.getOutputStream());
		so.writeInt(index);
		so.flush();
	}
	
	static boolean Connect(String IP, int Port)
	{
		    try {
		           sock =  new Socket(IP, Port);
		           return true;
		    }
		    catch (IOException e) {
		        System.out.println(e);
		        return false;
		    }
	}
	
	static int getInt() throws IOException
	{
		try {
			DataInputStream in = new DataInputStream(sock.getInputStream());
			return in.readInt();
		}catch(NullPointerException e)
		{
			return 0;
		}
	}
	
	static boolean getBoolean() throws IOException
	{
		try {
			DataInputStream in = new DataInputStream(sock.getInputStream());
			return in.readBoolean();
		}catch(NullPointerException e)
		{
			return false;
		}
	}
	
	static TreeSet<Integer> getBombs(int amount) throws IOException
	{
		TreeSet<Integer> bombs = new TreeSet<Integer>();
		try {
			
			for(int i = 0; i < amount; i++)
			{
				bombs.add(getInt());
			}
			return bombs;
		}catch(NullPointerException e)
		{
			return null;
		}
	}
	static boolean isConnected()
	{
		if(sock != null && sock.isConnected())
			return true;
		else
			return false;
	}
}
