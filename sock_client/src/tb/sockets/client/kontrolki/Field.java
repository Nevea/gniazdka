/**
 * 
 */
package tb.sockets.client.kontrolki;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.JButton;

/**
 * @author tb
 *
 */
@SuppressWarnings("serial")
public class Field extends JButton implements ActionListener{
	
	public enum Type{
		EMPTY, BOMB, NUMBER
	}
	
	Type fieldType = Type.EMPTY;
	
	
	private int index = 0;
	public boolean hidden = true;
	private Integer number = 0;
	public boolean isClicked = false;
	public boolean allowed = false;
	
	public Field() {
		super("");
		this.setMargin(new Insets(0, 0, 0, 0));
		this.setBackground(getBackground());
		this.addActionListener(this);
		super.setFont(super.getFont().deriveFont(Font.BOLD, 16f));
		fieldType = Type.EMPTY;
	}
	
	public void show()
	{
		if(number.intValue() != 0)
			setText(number.toString());
		if(fieldType == Type.BOMB)
		{
			this.setBackground(Color.RED);
		}
		else
		{
			this.setBackground(Color.GRAY);
		}
		hidden = false;
	}
	
	public void setNumber(Integer _number)
	{
		number = _number;
		setFieldType(Type.NUMBER);
	}
	
	public Integer getNumber()
	{
		return number;
	}
	
	public void setText(String text)
	{
		int number = 0;
		try
		{
			number = Integer.parseInt(text);
		}catch(NumberFormatException e) {}
		
		switch(number)
		{
		case 1:
			super.setForeground(Color.BLACK);
			break;
		case 2:
			super.setForeground(Color.RED);
			break;
		case 3:
			super.setForeground(Color.GREEN);
			break;
		case 4:
			super.setForeground(Color.MAGENTA);
			break;
		case 5:
			super.setForeground(Color.ORANGE);
			break;
		case 6:
			super.setForeground(Color.PINK);
			break;
		case 7:
			super.setForeground(Color.CYAN);
			break;
		case 8:
			super.setForeground(Color.BLUE);
			break;
		}
		super.setText(text);
		
	}
	
	public void setFieldType(Type _fieldType)
	{
		fieldType = _fieldType;
		switch(fieldType)
		{
		case EMPTY:
			this.setBackground(getBackground());
			break;
		case BOMB:
			this.setBackground(getBackground());
			break;
		case NUMBER:
			this.setBackground(getBackground());
			break;
		}
	}
	
	public Type getFieldType()
	{
		return fieldType;
	}
	
	public boolean isBomb()
	{
		if(fieldType == Type.BOMB)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(allowed && hidden)
		{
			isClicked = true;
			show();
		}
			
	}
}
