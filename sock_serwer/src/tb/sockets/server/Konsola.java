package tb.sockets.server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.TreeSet;

public class Konsola {
	
	static int bombLimit = 30;
	static TreeSet<Integer> bombSet = new TreeSet<Integer>();
	
	private static ServerSocket sSock;
	private static Socket player1;
	private static Socket player2;
	
	public static void main(String[] args) {
		bombSet = randomBombs();
		try {
			System.out.println("------------Server------------");
			sSock = new ServerSocket(6666);
			System.out.println("Created new ServerSocket");
			player1 = sSock.accept();
			System.out.println("Player1 joined");
			sendInt(bombLimit, player1);
			System.out.println("Player1 - sending bomb amounts");
			sendBombs(player1);
			System.out.println("Player1 - sending bombs position");
			sendBoolean(true, player1);
			System.out.println("Player1 - starting first");
			sendInt(1, player1);
			player2 = sSock.accept();
			System.out.println("Player2 joined");
			sendInt(bombLimit, player2);
			System.out.println("Player2 - sending bomb amounts");
			sendBombs(player2);
			System.out.println("Player2 - sending bombs position");
			sendBoolean(false, player2);
			System.out.println("Player2 - starting second");
			sendInt(2, player2);
			sendInt(2, player1);
			while(true)
			{
				sendInt(getInt(player1), player2);
				System.out.println("player1->player2");
				sendInt(getInt(player2), player1);
				System.out.println("player2->player1");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static TreeSet<Integer> randomBombs()
	{
		Random generator = new Random();
		while(bombSet.size() < bombLimit)
		{
			bombSet.add(generator.nextInt(16*16));
		}
		return bombSet;
	}
	
	public static void sendBombs(Socket sock) throws IOException
	{
		for(Integer i: bombSet)
		{
			DataOutputStream so = new DataOutputStream(sock.getOutputStream());
			so.writeInt(i.intValue());
			so.flush();
		}
	}
	
	public static void sendInt(int i, Socket sock) throws IOException
	{
		DataOutputStream so = new DataOutputStream(sock.getOutputStream());
		so.writeInt(i);
		so.flush();
	}
	
	public static void sendBoolean(boolean bool, Socket sock) throws IOException
	{
		DataOutputStream so = new DataOutputStream(sock.getOutputStream());
		so.writeBoolean(bool);
		so.flush();
	}
	
	public static int getInt(Socket sock) throws IOException
	{
		try {
			DataInputStream in = new DataInputStream(sock.getInputStream());
			return in.readInt();
		}catch(NullPointerException e)
		{
			return 0;
		}
	}
}
